<?php
// Set Headers
header('HTTP/1.0 200 OK');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json;charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Origin: *', true);
header("Cache-Control: no-cache, must-revalidate");

if (isset($_POST['tag'])) {
	$tag = $_POST['tag'];
	$ch  = curl_init();

	curl_setopt($ch, CURLOPT_URL, "https://www.instagram.com/explore/tags/" . $tag . "/?__a=1");
	curl_setopt($ch, CURLOPT_HEADER, 0);

	curl_exec($ch);
	curl_close($ch);
} else {
	echo json_encode([ error => 'Ошибка запроса' ]);
}