import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Column extends Component {
  render() {
    const { displayType, size, children } = this.props;

    return <div className={`col-${displayType}-${size}`}>{children}</div>;
  }
}

Column.defaultProps = {
  displayType: 'md',
  size: 12
};

Column.propTypes = {
  displayType: PropTypes.string,
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};

export default Column;