import React, { Component } from 'react';

class Content extends Component {
  render() {
    const { children: content } = this.props;

    return(
      <section className="Content">
        <div className="container">
          { content }
        </div>
      </section>
    );
  }
}

export default Content;