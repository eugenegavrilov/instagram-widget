import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Navbar extends Component {
  render() {
    const { brand } = this.props;

    return(
      <header className="Navbar">
        <nav className="navbar navbar-default">
          <div className="container-fluid">
            <div className="navbar-header">
              <span className="navbar-brand">{brand}</span>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}

Navbar.defaultProps = {
  brand: 'Test Application'
};

Navbar.propTypes = {
  brand: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};

export default Navbar;