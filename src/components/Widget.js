import React, { Component } from 'react';
import PropTypes from 'prop-types';

import WidgetTags from './WidgetTags';
import WidgetUsers from './WidgetUsers';

import '../styles/widget.css';

class Widget extends Component {
  render() {
    const { id, type, count } = this.props;

    return(
      <article className="widget">
        { type === 'tag'
          ? <WidgetTags tag={id} count={count} />
          : <WidgetUsers user={id} count={count} /> 
        }
      </article>
    );
  }
}

Widget.defaultProps = {
  type: 'person',
  startFrom: 1,
  count: 9
};

Widget.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  type: PropTypes.oneOf([
    'tag',
    'person',
    'user'
  ]),
  startFrom: PropTypes.number,
  count: PropTypes.number
};

export default Widget;