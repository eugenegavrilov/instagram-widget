import React, { Component } from 'react';
import PropTypes from 'prop-types';

class WidgetTags extends Component {
  state = {
    data: {}
  }

  componentWillMount() {
    const url = 'http://inst.eugene-gavrilov.ru/api/tags';
    const formData = new FormData();

    formData.append('tag', this.props.tag);

    fetch(url, {
      method: 'POST',
      body: formData
    })
      .then(response => response.json())
      .then(json => {
        this.setState({ data: json });
      });
  }

  render() {
    const { data: { tag } } = this.state;

    console.log(tag)

    if (tag) {
      return(
        <div className="widget-users">
          <header className="widget-header">Images by tag #{tag.name}</header>
          <div className="widget-content">
            { tag.media.nodes.map((node, i) => {
              if (i <= this.props.count - 1) {
                return <img className="widget-image thumbnail"
                            src={node.thumbnail_src}
                            alt="thumbnail"
                            key={node.id} />
              }
            }) }
          </div>
          <footer className="widget-footer">
            <span>found {tag.media.count} images by #{tag.name}</span>
          </footer>
        </div>
      );
    } else {
      return <p>Loading...</p>
    };
  }
}

export default WidgetTags;