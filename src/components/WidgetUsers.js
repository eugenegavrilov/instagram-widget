import React, { Component } from 'react';

class WidgetUsers extends Component {
  state = {
    data: {}
  }

  componentWillMount() {
    const url = 'http://inst.eugene-gavrilov.ru/api/users';
    const formData = new FormData();

    formData.append('user', this.props.user);

    fetch(url, {
      method: 'POST',
      body: formData
    })
      .then(response => response.json())
      .then(json => {
        this.setState({ data: json });
      });
  }

  render() {
    const { data: { user } } = this.state;

    console.log(user)

    if (user) {
      return(
        <div className="widget-users">
          <header className="widget-header">
            <a href={`https://www.instagram.com/${user.username}/`}
               target="_blank"><strong>@{user.username}</strong></a>
          </header>
          <div className="widget-content">
            { user.media.nodes.map((node, i) => {
              if (i <= this.props.count - 1) {
                return <img className="widget-image thumbnail"
                            src={node.thumbnail_src}
                            alt="thumbnail"
                            key={node.id} />
              }
            }) }
          </div>
          <footer className="widget-footer">
            <span>folowers: {user.followed_by.count}</span>
          </footer>
        </div>
      );
    } else {
      return <p>Loading...</p>
    }
  }
}

export default WidgetUsers;