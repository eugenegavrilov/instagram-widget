import React, { Component } from 'react';

import Navbar from '../components/Navbar';
import Content from '../components/Content';
import Widget from '../components/Widget';

import Row from '../components/Row';
import Column from '../components/Column';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar brand={'Instagram Widgets'}/>

        <Content>
          <Row>
            <Column size={4}>
              <Widget id={'cats'} type={'tag'} />
            </Column>

            <Column size={4}>
              <Widget id={'fair_cats'} type={'person'} count={12} />
            </Column>

            <Column size={4}>
              <Widget id={'girls'} type={'tag'} />
            </Column>
          </Row>
        </Content>
      </div>
    );
  }
}

export default App;
