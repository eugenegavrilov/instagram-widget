function jsonp(uri) {
  return new Promise(function(resolve, reject) {
    const id = '_' + Math.round(10000 * Math.random());
    const callbackName = 'jsonp_callback_' + id;

    window[callbackName] = function(data) {
        delete window[callbackName];
        const elem = document.getElementById(id);

        elem.parentNode.removeChild(elem);
        resolve(data);
    };

    const src = uri + '&callback=' + callbackName;
    let script = document.createElement('script');
    script.src = src;
    script.id = id;
    script.addEventListener('error', reject);
    (document.getElementsByTagName('head')[0] || document.body || document.documentElement).appendChild(script);
  });
}

export default jsonp;